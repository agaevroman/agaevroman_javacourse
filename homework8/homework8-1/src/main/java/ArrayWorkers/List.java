package ArrayWorkers;

import Exceptions.IllegalItemException;

public interface List<T> {
    T[] addItem(T[] array, T item) throws IllegalItemException;
    T[] removeItem(T[] array, T item) throws IllegalItemException;
    T[] removeAll(T[] array);
    int indexOf(T[] array, T item) throws IllegalItemException;
    boolean contains(T[] array, T item);
    int getCountOfElems(T[] array);
}

package ArrayWorkers;

import Exceptions.IllegalItemException;
import Resources.ExceptionMessages;

public class ArrayWorker<T> implements List<T> {

    public T[] addItem(T[] array, T item) throws IllegalItemException {
        @SuppressWarnings("unchecked")
        T[] newArray = (T[]) new Object[array.length + 1];

        if (contains(array, item)) {
            throw new IllegalItemException(ExceptionMessages.ELEMENT_IS_ALSO_IN_LIST);
        } else if (getCountOfElems(array) == array.length - 1 || array.length == 0) {
            throw new IllegalItemException(ExceptionMessages.YOU_CANT_ADD_MORE_ITEMS);
        } else {
            System.arraycopy(array, 0, newArray, 0, array.length);
        }

        newArray[newArray.length - 1] = item;

        return newArray;
    }

    public T[] removeItem(T[] array, T item) throws IllegalItemException {
        @SuppressWarnings("unchecked")
        T[] newArray = (T[]) new Object[array.length - 1];

        if (!contains(array, item)) {
            throw new IllegalItemException(ExceptionMessages.THERE_IS_NO_SUCH_ELEMENT);
        } else {
            for (int i = 0; i < newArray.length; i++) {
                if (array[i].equals(item)) {
                    continue;
                }

                newArray[i] = array[i];
            }
        }

        return newArray;
    }

    public T[] removeAll(T[] array) {
        return (T[]) new Object[array.length];
    }

    public int indexOf(T[] array, T item) throws IllegalItemException {
        if (!contains(array, item)) {
            throw new IllegalItemException(ExceptionMessages.THERE_IS_NO_SUCH_ELEMENT);
        }

        for (int i = 0; i < array.length; i++) {
            if (item.equals(array[i])) {
                return i;
            }
        }

        return 0;
    }

    public boolean contains(T[] array, T item) {
        for (T arrayItem : array) {
            if (arrayItem.equals(item)) {
                return true;
            }
        }

        return false;
    }

    public int getCountOfElems(T[] array) {
        int countOfElems = 0;
        for (T item : array) {
            if (item != null) {
                countOfElems++;
            }
        }

        return countOfElems;
    }
}

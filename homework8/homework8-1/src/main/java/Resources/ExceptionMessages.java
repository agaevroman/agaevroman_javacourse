package Resources;

public final class ExceptionMessages {
    public static final String THERE_IS_NO_SUCH_ELEMENT = "There is no such element here";
    public static final String ELEMENT_IS_ALSO_IN_LIST = "Element is also in list";
    public static final String YOU_CANT_ADD_MORE_ITEMS = "You can't add more items";
    public static final String INVALID_VALUE = "Invalid value";
}

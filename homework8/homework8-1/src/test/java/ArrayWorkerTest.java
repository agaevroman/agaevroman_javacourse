import ArrayWorkers.ArrayWorker;
import Exceptions.IllegalItemException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayWorkerTest {
    private final ArrayWorker<Object> arrayWorker = new ArrayWorker<>();
    private final Object[] testArray1 = {"1","2","3","4","5"};
    private final Object[] testArray2 = new Object[0];
    private final Object[] testArray3 = {"1"};

    @Test
    public void testAddItemShouldWorkAsExpected() throws IllegalItemException {
        Object[] actual = arrayWorker.addItem(testArray1, "6");

        Object[] expected = {"1","2","3","4","5","6"};

        assertArrayEquals(actual, expected);
    }

    @Test
    public void testAddItemShouldThrowExceptionIfArrayContainsAddedItem() {
        assertThrows(IllegalItemException.class, () -> arrayWorker.addItem(testArray1, "1"));
    }

    @Test
    public void testAddItemShouldThrowExceptionIfArrayIsFull(){
        assertThrows(IllegalItemException.class, () -> arrayWorker.addItem(testArray2, "1"));
    }

    @Test
    public void testRemoveItemShouldWorkAsExpected() throws IllegalItemException {
        Object[] actual = arrayWorker.removeItem(testArray3, "1");

        assertArrayEquals(actual, testArray2);
    }

    @Test
    public void testRemoveItemShouldThrowExceptionIfArrayNotContainsRemovingItem(){
        assertThrows(IllegalItemException.class, () -> arrayWorker.removeItem(testArray1, "9"));
    }

    @Test
    public void testRemoveAllShouldWorkAsExpected(){
        Object[] actual = arrayWorker.removeAll(testArray1);

        Object[] expected = new Object[testArray1.length];

        assertArrayEquals(actual, expected);
    }

    @Test
    public void testContainsShouldWorkAsExpected(){
        boolean actual = arrayWorker.contains(testArray1, "1");

        assertTrue(actual);
    }

    @Test
    public void testIndexOfShouldWorkAsExpected() throws IllegalItemException{
        int actualIndex = arrayWorker.indexOf(testArray1, "1");

        int expectedIndex = 0;

        assertEquals(actualIndex, expectedIndex);
    }

    @Test
    public void testIndexOfShouldThrowExceptionIfArrayNotContainsItem(){
        assertThrows(IllegalItemException.class, () -> arrayWorker.indexOf(testArray1, "10"));
    }
}

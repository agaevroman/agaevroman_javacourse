import GroupChooseService.EmployeeGroupService;
import Models.Employee;
import Models.EmployeesGroup;
import Models.EmployeesSkills;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeGroupServiceTest {
    private final EmployeeGroupService employeeGroupService = new EmployeeGroupService();
    private final ArrayList<Employee> employees = new ArrayList<>();

    @Test
    public void testCreateEmployeesGroupWorkAsExpected(){
        employees.add(new Employee(EmployeesSkills.FIRST_SKILL));
        employees.add(new Employee(EmployeesSkills.SECOND_SKILL));

        Employee employee1 = new Employee(EmployeesSkills.FIRST_SKILL);
        Employee employee2 = new Employee(EmployeesSkills.SECOND_SKILL);

        EmployeesGroup actual = employeeGroupService.createEmployeesGroup(employee1, employee2,20);
        EmployeesGroup expected = new EmployeesGroup(employees, 20);

        assertEquals(actual, expected);
    }

    @Test
    public void testCreateEmployeeWithSkillWorkAsExpected(){
        Employee actual = employeeGroupService.createEmployeeWithSkill(EmployeesSkills.FIRST_SKILL);

        Employee expected = new Employee(EmployeesSkills.FIRST_SKILL);

        assertEquals(actual, expected);
    }
}

package GroupChooseService;

import Models.Employee;
import Models.EmployeesSkills;
import Models.EmployeesGroup;

import java.util.ArrayList;

public class EmployeesGroupChooseService {
    public boolean isGroupApproach(EmployeesGroup employeesGroup, ArrayList<EmployeesSkills> requiredSkills, int requiredCost){
        ArrayList<EmployeesSkills> actualSkills = new ArrayList<>();
        for (Employee employer:employeesGroup.getEmployers()){
            actualSkills.add(employer.getEmployerSkills());
        }

        return actualSkills.equals(requiredSkills) && employeesGroup.getCost() <= requiredCost;
    }

    public void showIsGroupApproach(EmployeesGroup employeesGroup, ArrayList<EmployeesSkills> requiredSkills, int requiredCost){
        if(isGroupApproach(employeesGroup, requiredSkills, requiredCost)){

        }
        else{

        }
    }
}

package GroupChooseService;

import Models.Employee;
import Models.EmployeesGroup;
import Models.EmployeesSkills;

import java.util.ArrayList;

public class EmployeeGroupService {
   public EmployeesGroup createEmployeesGroup(Employee employee1, Employee employee2, int cost){
       ArrayList<Employee> employees = new ArrayList<>();
       employees.add(employee1);
       employees.add(employee2);

       return new EmployeesGroup(employees, cost);
   }

   public Employee createEmployeeWithSkill(EmployeesSkills employeeSkill){
       return new Employee(employeeSkill);
   }

   public ArrayList<EmployeesSkills> createRequiredSkillsArray(){
       ArrayList<EmployeesSkills> requiredSkills = new ArrayList<>();
       requiredSkills.add(EmployeesSkills.FIRST_SKILL);
       requiredSkills.add(EmployeesSkills.SECOND_SKILL);

       return requiredSkills;
   }
}

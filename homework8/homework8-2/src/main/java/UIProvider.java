import GroupChooseService.EmployeesGroupChooseService;
import Models.EmployeesSkills;
import Models.EmployeesGroup;
import Resources.OutputResources;

import java.util.ArrayList;

public class UIProvider {
    private final EmployeesGroupChooseService employersGroupChooseService = new EmployeesGroupChooseService();

    public void showMessage(String message){
        System.out.print(message);
    }

    public void showBuildInfo(EmployeesGroup employersGroup, ArrayList<EmployeesSkills> requiredSkills, int requiredCost){
        if(employersGroupChooseService.isGroupApproach(employersGroup, requiredSkills, requiredCost)){
            showMessage(OutputResources.THIS_GROUP_APPROACH);
        }
        else{
            showMessage(OutputResources.THIS_GROUP_NOT_APPROACH);
        }
    }
}

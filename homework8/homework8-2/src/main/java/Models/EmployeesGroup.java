package Models;

import java.util.ArrayList;

public class EmployeesGroup {
    private final ArrayList<Employee> employees;
    private final int cost;

    public EmployeesGroup(ArrayList<Employee> employees, int cost){
        this.employees = employees;
        this.cost = cost;
    }

    public ArrayList<Employee> getEmployers() {
        return employees;
    }

    public int getCost() {
        return cost;
    }
}

package Models;

public class Employee {
    private final EmployeesSkills employerSkills;

    public Employee(EmployeesSkills employeeSkills){
        this.employerSkills = employeeSkills;
    }

    public EmployeesSkills getEmployerSkills() {
        return employerSkills;
    }
}

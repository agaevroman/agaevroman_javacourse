import GroupChooseService.EmployeeGroupService;
import GroupChooseService.EmployeesGroupChooseService;
import Models.Employee;
import Models.EmployeesGroup;
import Models.EmployeesSkills;
import Resources.NumbersResources;
import Resources.OutputResources;

import java.util.ArrayList;

public class Main {
    private static final EmployeeGroupService employeeGroupService = new EmployeeGroupService();

    public static void main(String[] args){
        Employee employee1 = employeeGroupService.createEmployeeWithSkill(EmployeesSkills.FIRST_SKILL);
        Employee employee2 = employeeGroupService.createEmployeeWithSkill(EmployeesSkills.SECOND_SKILL);

        EmployeesGroup employeesGroup1 = employeeGroupService.createEmployeesGroup(employee1, employee2, 20);

        Employee employee3 = employeeGroupService.createEmployeeWithSkill(EmployeesSkills.FIRST_SKILL);
        Employee employee4 = employeeGroupService.createEmployeeWithSkill(EmployeesSkills.THIRD_SKILL);

        EmployeesGroup employeesGroup2 = employeeGroupService.createEmployeesGroup(employee3, employee4, 50);

        ArrayList<EmployeesSkills> requiredSkills = employeeGroupService.createRequiredSkillsArray();

        UIProvider uiProvider = new UIProvider();

        uiProvider.showMessage(OutputResources.GROUP_1);
        uiProvider.showBuildInfo(employeesGroup1, requiredSkills, NumbersResources.REQUIRED_COST);
        uiProvider.showMessage(OutputResources.GROUP_2);
        uiProvider.showBuildInfo(employeesGroup2, requiredSkills, NumbersResources.REQUIRED_COST);
    }
}

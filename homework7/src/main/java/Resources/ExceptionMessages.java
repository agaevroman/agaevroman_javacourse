package Resources;

public final class ExceptionMessages {
    public static final String VALIDATION_FAILED = "Validation failed\n";
    public static final String INCORRECT_INPUT = "Incorrect input, try string or number";
}

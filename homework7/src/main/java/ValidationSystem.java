import Validators.IntegerValidator;
import Validators.StringValidator;
import Exception.ValidationFailedException;
import Resources.ExceptionMessages;

public final class ValidationSystem {
    public static <T> void validate(T value) throws ValidationFailedException {
        if(value instanceof String){
            if(!new StringValidator().isValid(value.toString())){
                throw new ValidationFailedException(ExceptionMessages.VALIDATION_FAILED);
            }
        }
        else if(value instanceof Integer){
            if(!new IntegerValidator().isValid((Integer) value)){
                throw new ValidationFailedException(ExceptionMessages.VALIDATION_FAILED);
            }
        }
        else{
            throw new ValidationFailedException(ExceptionMessages.INCORRECT_INPUT);
        }
    }
}

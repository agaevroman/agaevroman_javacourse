package Validators;

public class IntegerValidator implements Validator<Integer>{
    public boolean isValid(Integer value){
        return value >= 1 && value <= 10;
    }
}

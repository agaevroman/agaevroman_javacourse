import Fibonacci.*;
import exceptionProviders.FibonacciExceptionProvider;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class FibonacciTest
{
    @ParameterizedTest
    @ArgumentsSource(FibonacciExceptionProvider.class)
    public void testCalculateFibonacciRowWithWhileWorks(int countOfNumbers, int[] expected){
        int[] real = new FibonacciWithWhile().getFibonacciRow(countOfNumbers);

        assertArrayEquals(real, expected);
    }

    @ParameterizedTest
    @ArgumentsSource(FibonacciExceptionProvider.class)
    public void testCalculateFibonacciRowWithDoWhileWorks(int countOfNumbers, int[] expected){
        int[] real = new FibonacciWithDoWhile().getFibonacciRow(countOfNumbers);

        assertArrayEquals(real, expected);
    }

    @ParameterizedTest
    @ArgumentsSource(FibonacciExceptionProvider.class)
    public void testCalculateFibonacciRowWithForWorks(int countOfNumbers, int[] expected){
        int[] real = new FibonacciWithFor().getFibonacciRow(countOfNumbers);

        assertArrayEquals(real, expected);
    }
}

import Handlers.ArrayHandler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayHandlerTest {
    @Test
    public void testToGetStringFromIntArrayWorks() {
        int[] array = {8, 2, 3, 90, 12};
        String expected = "8, 2, 3, 90, 12";

        String real = ArrayHandler.getStringFromIntArray(array);
        assertEquals(expected, real);
    }
}

package exceptionProviders;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class InvalidValues implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                (Object) new String[]{"3", "2", "5"},
                (Object) new String[]{"1", "7", "4"},
                (Object) new String[]{"1", "3", "-2"},
                (Object) new String[]{"-3", "1", "4"},
                (Object) new String[]{"2", "-4", "6"},
                (Object) new String[]{"1", "2", "-4"}
        ).map(Arguments::of);
    }
}

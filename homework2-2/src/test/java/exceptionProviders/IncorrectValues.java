package exceptionProviders;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IncorrectValues implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                (Object) new String[]{"3", "fgr", "5"},
                (Object) new String[]{"yhj", "2", "4"},
                (Object) new String[]{"1", "3", "3,ff"}).map(Arguments::of);
    }
}

package exceptionProviders;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IncorrectLengthValues implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                (Object)  new String[]{"2"},
                (Object) new String[]{"2", "7"},
                (Object) new String[]{"5", "1", "2.7", "4"},
                (Object) new String[]{"0", "6", "2.3", "6.1", "7.3"}).map(Arguments::of);
    }
}

import Factorial.*;
import exceptionProviders.FactorialExceptionProvider;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactorialTest {
    @ParameterizedTest
    @ArgumentsSource(FactorialExceptionProvider.class)
    public void testCalculateFactorialWithWhileWorks(int value, int expected) {
        int real = new FactorialWithWhile().getFactorialValue(value);

        assertEquals(real, expected);
    }

    @ParameterizedTest
    @ArgumentsSource(FactorialExceptionProvider.class)
    public void testCalculateFactorialWithDoWhileWorks(int value, int expected) {
        int real = new FactorialWithDoWhile().getFactorialValue(value);

        assertEquals(real, expected);
    }

    @ParameterizedTest
    @ArgumentsSource(FactorialExceptionProvider.class)
    public void testCalculateFactorialWithForWorks(int value, int expected) {
        int real = new FactorialWithFor().getFactorialValue(value);

        assertEquals(real, expected);
    }
}

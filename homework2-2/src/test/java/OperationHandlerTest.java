import Handlers.OperationsHandler;
import exceptionProviders.IncorrectLengthValues;
import exceptionProviders.IncorrectValues;
import exceptionProviders.InvalidValues;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OperationHandlerTest
{
    public static final String[] VALID_PARAMS = {"1","2","5"};

    @Test
    public void testOperationProcessingWorks(){
        OperationsHandler operationsHandler = new OperationsHandler(VALID_PARAMS);

        String real = operationsHandler.operationProcessing();

        assertNotEquals(real.length(), 0);
    }

    @Test
    public void testConstructorNotThrowExceptionWhenParamsAreValid(){
        assertDoesNotThrow(() -> new OperationsHandler(VALID_PARAMS));
    }

    @ParameterizedTest
    @ArgumentsSource(InvalidValues.class)
    public void testConstructorThrowIllegalArgumentExceptionWhenValueInvalid(String[] params){
        assertThrows(IllegalArgumentException.class, () -> new OperationsHandler(params));
    }

    @ParameterizedTest
    @ArgumentsSource(IncorrectLengthValues.class)
    public void testConstructorThrowIllegalArgumentExceptionWhenLengthIsInvalid(String[] params){
        assertThrows(IllegalArgumentException.class, () -> new OperationsHandler(params));
    }

    @ParameterizedTest
    @ArgumentsSource(IncorrectValues.class)
    public void testConstructorThrowNumberFormatException(String[] params){
        assertThrows(NumberFormatException.class, () -> new OperationsHandler(params));
    }
}

package Handlers;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ArrayHandler {
    public static String getStringFromIntArray(int[] array) {
        return Arrays.stream(array).mapToObj(String::valueOf).collect(Collectors.joining(", "));
    }
}

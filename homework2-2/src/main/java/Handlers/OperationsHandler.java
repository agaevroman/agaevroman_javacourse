package Handlers;

import Factorial.*;
import Fibonacci.*;
import Resources.Resources;
import Types.CycleType;
import Types.OperationTypes;

public class OperationsHandler {
    private int operationType;
    private int cycleType;
    private int value;
    private Factorial _factorial;
    private Fibonacci _fibonacci;

    public OperationsHandler(String[] params) {
        validateValues(params);
    }

    public String operationProcessing() {
        String result = " ";

        switch (operationType) {
            case (OperationTypes.FIBONACCI) -> {
                int[] fibonacciNumbers = _fibonacci.getFibonacciRow(value);
                result = ArrayHandler.getStringFromIntArray(fibonacciNumbers);
            }
            case (OperationTypes.FACTORIAL) -> {
                int factorialValue = _factorial.getFactorialValue(value);
                result = Integer.toString(factorialValue);
            }
        }

        return result;
    }

    private void validateValues(String[] params) throws IllegalArgumentException,NumberFormatException {
        if (!validateLength(params)) {
            throw new IllegalArgumentException(Resources.INPUT_3_NUMBERS);
        }
        if (!parseValues(params)) {
            throw new NumberFormatException(Resources.INVALID_INPUT);
        }
        if (!checkForRestrictions()) {
            throw new NumberFormatException(Resources.INCORRECT_INPUT);
        }

        checkForFactorialCycleType(cycleType);
        checkForFibonacciCycleType(cycleType);
    }

    private boolean validateLength(String[] params) throws IllegalArgumentException {
        return params.length == Resources.NEEDED_LENGTH;
    }

    private boolean parseValues(String[] params) throws NumberFormatException {
        try {
            operationType = Integer.parseInt(params[0]);
            cycleType = Integer.parseInt(params[1]);
            value = Integer.parseInt(params[2]);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    private boolean checkForRestrictions() {
        if (operationType > OperationTypes.FACTORIAL || operationType < OperationTypes.FIBONACCI) {
            return false;
        }

        if (cycleType > CycleType.FOR || cycleType < CycleType.WHILE) {
            return false;
        }

         return value >= 0;
    }

    private void checkForFactorialCycleType(int cycleType) {
        if (cycleType == CycleType.WHILE) {
            _factorial = new FactorialWithWhile();
        }
        if (cycleType == CycleType.DO_WHILE) {
            _factorial = new FactorialWithDoWhile();
        }
        if (cycleType == CycleType.FOR) {
            _factorial = new FactorialWithFor();
        }
    }

    private void checkForFibonacciCycleType(int cycleType) {
        if (cycleType == CycleType.WHILE) {
            _fibonacci = new FibonacciWithWhile();
        }
        if (cycleType == CycleType.DO_WHILE) {
            _fibonacci = new FibonacciWithDoWhile();
        }
        if (cycleType == CycleType.FOR) {
            _fibonacci = new FibonacciWithFor();
        }
    }
}
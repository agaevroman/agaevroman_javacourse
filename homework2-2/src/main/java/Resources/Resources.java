package Resources;

public class Resources {
    public static final int NEEDED_LENGTH = 3;
    public static final String INPUT_3_NUMBERS = "Input 3 numbers";
    public static final String INVALID_INPUT = "Invalid input, try again";
    public static final String INCORRECT_INPUT = "Inputted numbers are more than required number for function select";
    public static final String RESULT = "Result is:";
}

import Handlers.OperationsHandler;
import Resources.Resources;

public class Main {
    public static void main(String[] args) {
        try {
            OperationsHandler operationsHandler = new OperationsHandler(args);
            System.out.print(Resources.RESULT);
            System.out.print(operationsHandler.operationProcessing());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
package Factorial;

public class FactorialWithFor implements Factorial {
    public int getFactorialValue(int number) {
        int factorialValue = 1;

        for (int i = 2; i <= number; i++) {
            factorialValue *= i;
        }

        return factorialValue;
    }
}

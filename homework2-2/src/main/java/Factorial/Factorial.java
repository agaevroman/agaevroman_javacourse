package Factorial;

public interface Factorial
{
    int getFactorialValue(int number);
}

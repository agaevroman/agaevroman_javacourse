package Factorial;

public class FactorialWithWhile implements Factorial {
    public int getFactorialValue(int number) {
        int factorialValue = 1;
        int counter = 1;

        while (counter <= number) {
            factorialValue *= counter;
            counter++;
        }

        return factorialValue;
    }
}

package Factorial;

public class FactorialWithDoWhile implements Factorial {
    public int getFactorialValue(int number) {
        int factorialValue = 1;
        int counter = 1;

        do {
            factorialValue *= counter;
            counter++;
        }
        while (counter <= number);

        return factorialValue;
    }
}

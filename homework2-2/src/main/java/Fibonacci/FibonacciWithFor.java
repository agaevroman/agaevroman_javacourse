package Fibonacci;

public class FibonacciWithFor implements Fibonacci
{
    public int[] getFibonacciRow(int countOfNumbers)
    {
        int[] fibonacciNumbers = new int[countOfNumbers];

        int x1 = 1;
        int x2 = 0;

        for (int i = 1; i <= countOfNumbers; ++i) {
            fibonacciNumbers[i - 1] = x2;

            int temp = x1 + x2;
            x1 = x2;
            x2 = temp;
        }

        return fibonacciNumbers;
    }
}

package Fibonacci;

public class FibonacciWithWhile implements Fibonacci {
    public int[] getFibonacciRow(int countOfNumbers) {
        int[] fibonacciNumbers = new int[countOfNumbers];

        int i = 1;
        int x1 = 1;
        int x2 = 0;

        while (i <= countOfNumbers) {
            fibonacciNumbers[i - 1] = x2;

            int temp = x1 + x2;
            x1 = x2;
            x2 = temp;

            i++;
        }

        return fibonacciNumbers;
    }
}

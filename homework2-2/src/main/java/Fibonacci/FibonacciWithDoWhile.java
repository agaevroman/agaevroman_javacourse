package Fibonacci;

public class FibonacciWithDoWhile implements Fibonacci
{
    public int[] getFibonacciRow(int countOfNumbers)
    {
        int[] fibonacciNumbers = new int[countOfNumbers];

        int x1 = 1;
        int x2 = 0;
        int i = 0;

        do {
            fibonacciNumbers[i] = x2;

            int temp = x1 + x2;
            x1 = x2;
            x2 = temp;

            i++;
        } while (i < countOfNumbers);

        return fibonacciNumbers;
    }
}

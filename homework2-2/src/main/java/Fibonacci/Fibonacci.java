package Fibonacci;

public interface Fibonacci
{
    int[] getFibonacciRow(int countOfNumbers);
}

package ExceptionProviders;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IncorrectLengthParams implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                (Object) new String[]{"3"},
                (Object) new String[]{"3", "7"},
                (Object) new String[]{"4", "6", "2.3"},
                (Object) new String[]{"3", "10", "2.3", "5.1", "2.3"},
                (Object) new String[]{"12", "6", "2.3", "2.1", "2.3", "9"}).map(Arguments::of);
    }
}

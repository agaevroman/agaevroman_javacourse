package ExceptionProviders;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class InvalidParams implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                (Object) new String[]{"1", "5", "0", "0"},
                (Object) new String[]{"3", "0", "4.3", "5.3"},
                (Object) new String[]{"3", "0", "-6.3", "6.3"},
                (Object) new String[]{"9", "0", "7.3", "-7.3"}).map(Arguments::of);
    }
}


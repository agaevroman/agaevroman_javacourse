import ExceptionProviders.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.*;

public class ExpressionTest {

    private final String[] VALID_PARAMETERS = new String[]{"3", "2", "4.3", "5.3"};

    @Test
    public void testCalculateShouldNotThrowIllegalArgumentException() {
        Expression expression = new Expression(VALID_PARAMETERS);
        assertDoesNotThrow(expression::calculate);
    }

    @ParameterizedTest
    @ArgumentsSource(InvalidParams.class)
    public void testConstructorShouldThrowIllegalArgumentExceptionWhenParametersInvalid(String[] parameters) {
        assertThrows(IllegalArgumentException.class, () -> new Expression(parameters));
    }

    @ParameterizedTest
    @ArgumentsSource(IncorrectLengthParams.class)
    public void testConstructorShouldThrowIllegalArgumentExceptionWhenLengthNotEqualFour(String[] parameters) {
        assertThrows(IllegalArgumentException.class, () -> new Expression(parameters));
    }

    @ParameterizedTest
    @ArgumentsSource(IncorrectParams.class)
    public void testConstructorShouldThrowNumberFormatException(String[] parameters) {
        assertThrows(NumberFormatException.class, () -> new Expression(parameters));
    }

    @Test
    public void testConstructorShouldNotThrowIllegalArgumentOrNumberFormatException() {
        assertDoesNotThrow(() -> new Expression(VALID_PARAMETERS));
    }
}

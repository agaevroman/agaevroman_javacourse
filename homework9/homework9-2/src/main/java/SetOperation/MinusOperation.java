package SetOperation;

import java.util.HashSet;

public class MinusOperation implements Operations{
    @Override
    public HashSet<Integer> operation(HashSet<Integer> firstSet, HashSet<Integer> secondSet) {
        HashSet<Integer> newSet = new HashSet<>();

        for(int item:firstSet){
            if(secondSet.contains(item)){
                continue;
            }
            newSet.add(item);
        }

        return newSet;
    }
}

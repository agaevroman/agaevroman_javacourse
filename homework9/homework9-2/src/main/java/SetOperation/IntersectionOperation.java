package SetOperation;

import java.util.ArrayList;
import java.util.HashSet;

public class IntersectionOperation implements Operations{

    @Override
    public HashSet<Integer> operation(HashSet<Integer> firstSet, HashSet<Integer> secondSet) {
        HashSet<Integer> newSet = new HashSet<>();

        for(int item:firstSet){
            if(secondSet.contains(item)){
                newSet.add(item);
            }
        }

        for(int item:secondSet){
            if(firstSet.contains(item) && !newSet.contains(item)){
                newSet.add(item);
            }
        }

        return newSet;
    }
}

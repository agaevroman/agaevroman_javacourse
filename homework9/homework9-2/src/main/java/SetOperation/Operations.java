package SetOperation;

import java.util.HashSet;

public interface Operations {
   public HashSet<Integer> operation(HashSet<Integer> firstSet, HashSet<Integer> secondSet);
}

package SetOperation;

import java.util.HashSet;

public class UnionOperation implements Operations {
    @Override
    public HashSet<Integer> operation(HashSet<Integer> firstSet, HashSet<Integer> secondSet) {

        HashSet<Integer> newSet = new HashSet<>(firstSet);

        for (int item : secondSet) {
            if (!newSet.contains(item)) {
                newSet.add(item);
            }
        }

        return newSet;
    }
}

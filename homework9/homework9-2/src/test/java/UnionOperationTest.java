import SetOperation.UnionOperation;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnionOperationTest {
    private final UnionOperation unionOperation = new UnionOperation();
    private final HashSet<Integer> testSet1 = new HashSet<>();
    private final HashSet<Integer> testSet2 = new HashSet<>();

    @Test
    public void testUnionSetsOperationWorkAsExpected(){
        testSet1.add(1);
        testSet1.add(2);
        testSet2.add(2);
        testSet2.add(3);

        HashSet<Integer> actual = unionOperation.operation(testSet1, testSet2);

        HashSet<Integer> expected = new HashSet<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);

        assertEquals(actual, expected);
    }
}

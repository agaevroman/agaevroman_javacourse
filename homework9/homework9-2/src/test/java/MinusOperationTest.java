import SetOperation.MinusOperation;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MinusOperationTest {
    private final MinusOperation minusOperation = new MinusOperation();
    private final HashSet<Integer> testSet1 = new HashSet<>();
    private final HashSet<Integer> testSet2 = new HashSet<>();

    @Test
    public void testMinusOperationWorkAsExpected(){
        testSet1.add(1);
        testSet1.add(2);
        testSet2.add(3);
        testSet2.add(2);

        HashSet<Integer> actual = minusOperation.operation(testSet1, testSet2);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(1);

        assertEquals(actual, expected);
    }
}

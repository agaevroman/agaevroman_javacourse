import SetOperation.IntersectionOperation;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class IntersectionOperationTest {
    private final IntersectionOperation intersectionOperation = new IntersectionOperation();
    private final HashSet<Integer> testSet1 = new HashSet<>();
    private final HashSet<Integer> testSet2 = new HashSet<>();

    @Test
    public void testIntersectionSetsOperationWorkAsExpected(){
        testSet1.add(1);
        testSet1.add(2);
        testSet2.add(2);
        testSet2.add(3);

        HashSet<Integer> actual = intersectionOperation.operation(testSet1, testSet2);
        HashSet<Integer> expected = new HashSet<>();

        expected.add(2);

        assertEquals(actual, expected);
    }
}

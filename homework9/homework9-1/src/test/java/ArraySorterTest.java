import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArraySorterTest {
    private final ArraySorter arraySorter = new ArraySorter();
    int[] testArray = {12, 89, 102, 304};

    @Test
    public void sortArrayWorkAsExpected() {
        int[] actual = arraySorter.sortArray(testArray);

        int[] expected = {12, 102, 304, 89};

        assertArrayEquals(actual, expected);
    }
}

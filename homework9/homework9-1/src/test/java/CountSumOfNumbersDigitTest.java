import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CountSumOfNumbersDigitTest {
    private final SumOfDigitsOfNumberHandler sumOfDigitsOfNumberHandler = new SumOfDigitsOfNumberHandler();

    @Test
    public void countSumOfDigitsWorkAsExpected(){
        int testNumber = 1234567890;
        int actual = sumOfDigitsOfNumberHandler.countSumOfNumberDigits(testNumber);

        int expected = 45;

        assertEquals(actual, expected);
    }
}

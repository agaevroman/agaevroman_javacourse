public class MenuFacade {
    public void run(){
        ArraySorter arraySorter = new ArraySorter();
        ArrayWorker arrayWorker = new ArrayWorker();

        int[] array = new int[10];

        arrayWorker.fillArrayRandomNumbers(array);

        System.out.print("Unsorted array\n");
        arrayWorker.showArray(array);

        int[] sortedArray = arraySorter.sortArray(array);

        System.out.print("\nSorted array\n");
        arrayWorker.showArray(sortedArray);
    }
}

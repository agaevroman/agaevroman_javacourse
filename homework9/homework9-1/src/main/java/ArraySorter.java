public class ArraySorter {
    private final SumOfDigitsOfNumberHandler sumOfDigitsOfNumberHandler = new SumOfDigitsOfNumberHandler();
    public int[] sortArray(int[] array){
        for (int i = array.length - 1;i > 0;i--){
            for (int j = 0;j < i;j++){
                if(sumOfDigitsOfNumberHandler.compare(array[j], array[j + 1]) > 0){
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        return array;
    }
}

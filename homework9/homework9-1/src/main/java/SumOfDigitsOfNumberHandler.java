import java.util.Comparator;

public class SumOfDigitsOfNumberHandler implements Comparator<Integer> {
    public Integer countSumOfNumberDigits(int number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }

        return sum;
    }

    @Override
    public int compare(Integer number1, Integer number2) {
        return countSumOfNumberDigits(number1).compareTo(countSumOfNumberDigits(number2));
    }
}

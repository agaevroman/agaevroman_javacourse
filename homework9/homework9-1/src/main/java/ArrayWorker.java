import java.util.Random;

public class ArrayWorker {
    public void fillArrayRandomNumbers(int[] array){
        Random random = new Random();

        for (int i = 0;i < array.length; i++){
            array[i] = random.nextInt(0,100);
        }
    }

    public void showArray(int[] array){
        for(int item:array){
            System.out.print(item + " ");
        }
    }
}

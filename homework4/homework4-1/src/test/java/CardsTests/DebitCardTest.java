package CardsTests;

import CreditCards.DebitCard;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DebitCardTest {
    private DebitCard debitCard;

    public DebitCardTest(){
        debitCard = new DebitCard("Roman", BigDecimal.valueOf(70));
    }

    @Test
    public void testAddMoneyShouldWorkAsExpected(){
        debitCard.addMoneyToBalance(BigDecimal.valueOf(20));

        BigDecimal actual = debitCard.getBalance();

        BigDecimal expected = BigDecimal.valueOf(90);

        assertEquals(actual, expected);
    }

    @Test
    public void testConvertMoneyShouldWorkAsExpected(){
        BigDecimal actual = debitCard.convertMoney(2.0);

        BigDecimal expected = BigDecimal.valueOf(140.0);

        assertEquals(actual, expected);
    }

    @Test
    public void testWithDrawMoneyShouldWorkIfMoneyLessThanCardBalance(){
        debitCard.withdrawMoney(BigDecimal.valueOf(69));

        BigDecimal actual = debitCard.getBalance();

        BigDecimal expected = BigDecimal.valueOf(1);

        assertEquals(actual, expected);
    }

    @Test
    public void testWithDrawMoneyShouldThrowExceptionIfMoneyMoreThanCardBalance(){
        assertThrows(NumberFormatException.class, () -> debitCard.withdrawMoney(BigDecimal.valueOf(71)));
    }
}

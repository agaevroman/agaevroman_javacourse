package CardsTests;

import CreditCards.CreditCard;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreditCardTest {
    private CreditCard creditCard;

    public CreditCardTest() {
        creditCard = new CreditCard("Bublik", BigDecimal.valueOf(12));
    }

    @Test
    public void testAddMoneyToBalanceShouldWorkAsExpected() {
        creditCard.addMoneyToBalance(new BigDecimal(50));

        BigDecimal actual = creditCard.getBalance();

        BigDecimal expected = new BigDecimal(62);

        assertEquals(actual, expected);
    }

    @Test
    public void testWithDrawMoneyShouldWorkAsExpected() {
        creditCard.withdrawMoney(new BigDecimal(2));

        BigDecimal actual = creditCard.getBalance();

        BigDecimal expected = new BigDecimal(10);

        assertEquals(actual, expected);
    }

    @Test
    public void testWithDrawMoneyShouldWorkIfMoneyMoreThanCardBalance() {
        creditCard.withdrawMoney(new BigDecimal(20));

        BigDecimal actual = creditCard.getBalance();

        BigDecimal expected = new BigDecimal(-8);

        assertEquals(actual, expected);
    }


    @Test
    public void testConvertMoneyShouldWorkAsExpected() {
        BigDecimal actual = creditCard.convertMoney(2);

        BigDecimal expected = BigDecimal.valueOf(24.0);

        assertEquals(actual, expected);
    }
}

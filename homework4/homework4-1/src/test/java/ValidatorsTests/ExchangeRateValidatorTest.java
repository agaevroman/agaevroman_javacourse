package ValidatorsTests;

import Validators.ExchangeRateValidator;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExchangeRateValidatorTest {
    @Test
    public void testIsValidShouldReturnTrue() {
        boolean actual = new ExchangeRateValidator().isValid(new BigDecimal(0));

        assertTrue(actual);
    }

    @Test
    public void testIsValidShouldReturnFalse() {
        boolean actual = new ExchangeRateValidator().isValid(new BigDecimal(1.3));

        assertFalse(actual);
    }
}

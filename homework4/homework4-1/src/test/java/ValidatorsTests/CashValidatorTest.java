package ValidatorsTests;

import Validators.CashValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CashValidatorTest {
    @Test
    public void testIsValidShouldReturnTrue(){
        boolean actual = new CashValidator().isValid(12.2);

        assertTrue(actual);
    }

    @Test
    public void testIsValidShouldReturnFalse() {
        boolean actual = new CashValidator().isValid(-1.2);

        assertFalse(actual);
    }

    @Test
    public void testIsParsedShouldReturnTrue(){
        boolean actual = new CashValidator().isParsed("12.34");

        assertTrue(actual);
    }

    @Test
    public void testIsParsedShouldReturnFalse(){
        boolean actual = new CashValidator().isParsed("12.ggf@22");

        assertFalse(actual);
    }
}

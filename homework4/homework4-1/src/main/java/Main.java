import CardsActions.*;
import ConsoleProviders.*;
import CreditCards.*;
import Validators.*;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        ExchangeRateValidator exchangeRateValidator = new ExchangeRateValidator();
        CashValidator cashValidator = new CashValidator();

        ConsoleReader consoleReader = new ConsoleReader(cashValidator, exchangeRateValidator);

        AddMoney addMoney = new AddMoney(consoleReader);
        ConvertMoney convertMoney = new ConvertMoney(consoleReader);
        WithdrawBalance withdrawBalance = new WithdrawBalance(consoleReader);

        UIProvider uiProvider = new UIProvider(consoleReader, addMoney, convertMoney, withdrawBalance);

        CreditCard creditCard = new CreditCard("Roman credit", new BigDecimal(12));
        DebitCard debitCard = new DebitCard("Roman debit", new BigDecimal(20));

        uiProvider.showMainMenu(creditCard);
    }
}

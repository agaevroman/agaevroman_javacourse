package CreditCards;

import java.math.BigDecimal;

import Resources.ExceptionMessages;

public class DebitCard extends Card {
        public DebitCard(String holder, BigDecimal balance) {
            super(holder, balance);
        }

        public DebitCard(String holder) {
            super(holder);
        }

    @Override
    public void withdrawMoney(BigDecimal money) throws NumberFormatException {
        if (money.compareTo(balance) > 0) {
            throw new NumberFormatException(ExceptionMessages.CANNOT_WITHDRAW_MONEY);
        }

        balance = balance.subtract(money);
    }
}

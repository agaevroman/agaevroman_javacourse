package CreditCards;

import java.math.BigDecimal;

public class CreditCard extends Card
{
    public CreditCard(String holder, BigDecimal balance) {
        super(holder, balance);
    }

    public CreditCard(String holder) {
        super(holder);
    }

    @Override
    public void withdrawMoney(BigDecimal money) {
        balance = balance.subtract((money));
    }
}

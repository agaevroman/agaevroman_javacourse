package CreditCards;

import java.math.BigDecimal;

public abstract class Card {
    private String holder;
    protected BigDecimal balance;

    public Card(String _holder, BigDecimal _balance) {
        holder = _holder;
        balance = _balance;
    }

    public Card(String _holder) {
        holder = _holder;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getHolder() {
        return holder;
    }

    public void addMoneyToBalance(BigDecimal money) {
        balance = balance.add(money);
    }

    public abstract void withdrawMoney(BigDecimal money);

    public BigDecimal convertMoney(double exchangeRate) {
        return balance.multiply(BigDecimal.valueOf(exchangeRate));
    }
}

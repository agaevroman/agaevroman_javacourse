package Resources;

public class OutputResources
{
    public static final String INPUT_AMOUNT_OF_MONEY = "Input amount of money";
    public static final String INPUT_EXCHANGE_RATE = "Input exchange rate";
    public static final String MAIN_MENU_ITEMS = "\n1.Show balance\n2.Add money to balance\n3.Get money from balance\n4.Convert money\n5.Exit";
    public static final String INPUT_MENU_ITEM = "\nInput menu item";
}

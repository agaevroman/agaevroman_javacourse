package Resources;

public class ExceptionMessages
{
    public static final String CANNOT_WITHDRAW_MONEY = "You can't withdraw money";
    public static final String INCORRECT_INPUT = "Incorrect input\n";
}

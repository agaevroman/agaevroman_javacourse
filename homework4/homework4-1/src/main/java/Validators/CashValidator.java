package Validators;

public class CashValidator implements Validator<Double> {
    @Override
    public boolean isValid(Double value) {
        return value > 0;
    }

    public boolean isParsed(String value) {
        try {
            double parsedValue = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}

package CardsActions;

import ConsoleProviders.ConsoleReader;
import CreditCards.Card;
import Resources.OutputResources;

import java.math.BigDecimal;

public class ConvertMoney {

    private ConsoleReader consoleReader;

    public ConvertMoney(ConsoleReader consoleReader) {
        this.consoleReader = consoleReader;
    }

    public BigDecimal convertMoney(Card card) {

        double exchangeRate = consoleReader.getExchangeRate(OutputResources.INPUT_EXCHANGE_RATE);

        return card.convertMoney(exchangeRate);
    }
}

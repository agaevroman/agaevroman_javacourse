package CardsActions;

import ConsoleProviders.ConsoleReader;
import CreditCards.Card;
import Resources.OutputResources;

import java.math.BigDecimal;

public class AddMoney {

    private ConsoleReader consoleReader;

    public AddMoney(ConsoleReader consoleReader){
        this.consoleReader = consoleReader;
    }

    public void addMoney(Card card) {
        BigDecimal money = BigDecimal.valueOf(consoleReader.getValue(OutputResources.INPUT_AMOUNT_OF_MONEY));

        card.addMoneyToBalance(money);
    }
}

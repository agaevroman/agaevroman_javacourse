package CardsActions;

import ConsoleProviders.ConsoleReader;
import CreditCards.Card;
import Resources.OutputResources;

import java.math.BigDecimal;

public class WithdrawBalance {
    private ConsoleReader consoleReader;

    public WithdrawBalance(ConsoleReader consoleReader){
        this.consoleReader = consoleReader;
    }

    public void withdrawBalance(Card card) {
        BigDecimal money = BigDecimal.valueOf(consoleReader.getValue(OutputResources.INPUT_AMOUNT_OF_MONEY));

        card.withdrawMoney(money);
    }
}

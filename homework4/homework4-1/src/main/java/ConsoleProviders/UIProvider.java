package ConsoleProviders;

import CardsActions.AddMoney;
import CardsActions.ConvertMoney;
import CardsActions.WithdrawBalance;
import CreditCards.Card;
import Resources.ExceptionMessages;
import Resources.OutputResources;

public class UIProvider {
    private ConsoleReader consoleReader;
    private AddMoney addMoney;
    private ConvertMoney convertMoney;
    private WithdrawBalance withdrawBalance;

    public UIProvider(ConsoleReader consoleReader, AddMoney addMoney, ConvertMoney convertMoney, WithdrawBalance withdrawBalance){
        this.consoleReader = consoleReader;
        this.addMoney = addMoney;
        this.convertMoney = convertMoney;
        this.withdrawBalance = withdrawBalance;
    }

    public void showMainMenu(Card card) {
        boolean isRun = true;

        while (isRun) {
            showMessage(OutputResources.MAIN_MENU_ITEMS);

            switch (consoleReader.getMenuItem(OutputResources.INPUT_MENU_ITEM)) {
                case 1 -> showMessage(card.getBalance().toString());
                case 2 -> addMoney.addMoney(card);
                case 3 -> withdrawBalance.withdrawBalance(card);
                case 4 -> convertMoney.convertMoney(card);
                case 5 -> isRun = false;
                default -> showMessage(ExceptionMessages.INCORRECT_INPUT);
            }
        }
    }

    public void showMessage(String message) {
        System.out.print(message);
    }
}

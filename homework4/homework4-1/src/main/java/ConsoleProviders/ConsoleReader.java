package ConsoleProviders;

import Validators.CashValidator;
import Resources.ExceptionMessages;
import Validators.ExchangeRateValidator;

import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleReader {

    private CashValidator cashValidator;
    private ExchangeRateValidator exchangeRateValidator;

    public ConsoleReader(CashValidator cashValidator, ExchangeRateValidator exchangeRateValidator) {
        this.cashValidator = cashValidator;
        this.exchangeRateValidator = exchangeRateValidator;
    }

    public String getText(String message) {
        System.out.println();
        Scanner scanner = new Scanner(System.in);

        System.out.print(message);

        return scanner.next();
    }

    public double getValue(String message) throws NumberFormatException {
        String value = getText(message);

        if (cashValidator.isParsed(value) && cashValidator.isValid(Double.parseDouble(value))) {
            return Double.parseDouble(value);
        }

        throw new NumberFormatException(ExceptionMessages.INCORRECT_INPUT);
    }

    public double getExchangeRate(String message) throws NumberFormatException {
        double value = getValue(message);

        if (exchangeRateValidator.isValid(BigDecimal.valueOf(value))) {
            return getValue(message);
        }

        throw new NumberFormatException(ExceptionMessages.INCORRECT_INPUT);
    }

    public int getMenuItem(String message) throws NumberFormatException {
        String value = getText(message);

        if (cashValidator.isParsed(value)) {
            return Integer.parseInt(value);
        }

        throw new NumberFormatException(ExceptionMessages.INCORRECT_INPUT);
    }
}

package SortTests;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class IntArrayProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        return Stream.of(
                Arguments.of(new int[]{1, -9, 3, 12},
                        new int[]{-9, 1, 3, 12},
                        Arguments.of(new int[]{0, 1, 3, -7, 12, 6, -90},
                                new int[]{-90, -7, 0, 1, 3, 6, 12}),
                        Arguments.of(new int[]{-1, -2, -10, 9, 127, 3, 14},
                                new int[]{-10, -2, -1, 3, 9, 14, 127})
                ));
    }
}

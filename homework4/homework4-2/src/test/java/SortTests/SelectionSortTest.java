package SortTests;

import ArraySorters.SelectionSort;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SelectionSortTest {
    private SelectionSort selectionSort;

    public SelectionSortTest(){
        this.selectionSort = new SelectionSort();
    }

    @ParameterizedTest
    @ArgumentsSource(IntArrayProvider.class)
    public void testSelectionSortWorkAsExpected(int[] unsortedArray, int[] sortedArray){
        selectionSort.sort(unsortedArray);
        assertArrayEquals(unsortedArray, sortedArray);
    }
}

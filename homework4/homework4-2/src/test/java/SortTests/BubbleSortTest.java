package SortTests;

import ArraySorters.BubbleSort;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class BubbleSortTest {
    private BubbleSort bubbleSort;

    public BubbleSortTest(){
        this.bubbleSort = new BubbleSort();
    }

    @ParameterizedTest
    @ArgumentsSource(IntArrayProvider.class)
    public void testSortWorkAsExpected(int[] unsortedArray, int[] sortedArray){
        bubbleSort.sort(unsortedArray);
        assertArrayEquals(unsortedArray, sortedArray);
    }
}

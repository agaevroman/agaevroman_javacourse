import ArraySorters.Sort;

public class SortingContext {
    private final Sort sortStrategy;

    public SortingContext(Sort sortStrategy){
        this.sortStrategy = sortStrategy;
    }

    public void execute(int[] array){
        sortStrategy.sort(array);
    }
}

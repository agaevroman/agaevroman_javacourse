package ArrayHelper;

import java.util.Random;

public class ArrayCreator
{
    public int[] createArray(){
        int[] array = new int[15];

        fillArrayWithRandomNumbers(array);

        return array;
    }

    public void fillArrayWithRandomNumbers(int[] array){
        Random random = new Random();

        for (int i = 0; i < array.length; i++){
            array[i] = random.nextInt(1,20);
        }
    }

    public void showArray(int[] array){
        for(int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
    }
}

package ArraySorters;

public abstract class Sort {
    public abstract void sort(int[] array);

    protected void swapElements(int[] array, int firstIndex, int secondIndex){
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}

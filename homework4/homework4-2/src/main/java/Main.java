import ArraySorters.SelectionSort;
import ArrayHelper.ArrayCreator;

public class Main {
    public static void main(String[] args){
        SortingContext sortingContext = new SortingContext(new SelectionSort());
        ArrayCreator arrayCreator = new ArrayCreator();

        int[] array = arrayCreator.createArray();
        System.out.println(Resources.UNSORTED_ARRAY);
        arrayCreator.showArray(array);

        sortingContext.execute(array);

        System.out.println();
        System.out.println(Resources.SORTED_ARRAY);
        arrayCreator.showArray(array);
    }
}

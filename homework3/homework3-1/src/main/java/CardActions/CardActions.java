package CardActions;

import Card.Card;
import ConsoleProviders.ConsoleReader;
import Resources.ExceptionMessages;
import Resources.OutputResources;

import java.math.BigDecimal;

public class CardActions {
    private ConsoleReader consoleReader = new ConsoleReader();

    public void addMoneyToBalance(Card card, BigDecimal money) {
        BigDecimal newBalance = card.getBalance().add(money);

        card.setBalance(newBalance);
    }

    public void withdrawMoneyFromBalance(Card card, BigDecimal money) throws IllegalArgumentException {
        BigDecimal newBalance = card.getBalance().subtract(money);

        if (newBalance.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException(ExceptionMessages.YOU_INPUT_MORE_MONEY_THAN_CAN_WITHDRAW);
        }

        card.setBalance(newBalance);
    }

    public BigDecimal getConvertedMoney(Card card, BigDecimal exchangeRate) {
        return card.getBalance().multiply(exchangeRate);
    }
}

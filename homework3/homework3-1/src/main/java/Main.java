import Card.Card;
import ConsoleProviders.UIProvider;

import java.math.BigDecimal;

public class Main{
    public static void main(String[] args){
        Card card = new Card("Bublik", BigDecimal.valueOf(50));
        UIProvider uiProvider = new UIProvider();

        uiProvider.showMainMenu(card);
    }
}
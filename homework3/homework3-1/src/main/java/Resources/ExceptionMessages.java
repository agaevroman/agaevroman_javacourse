package Resources;

public final class ExceptionMessages {
    public static final String INCORRECT_INPUT = "Incorrect input, try once more";
    public static final String YOU_INPUT_MORE_MONEY_THAN_CAN_WITHDRAW = "You input more money than can withdraw";
}

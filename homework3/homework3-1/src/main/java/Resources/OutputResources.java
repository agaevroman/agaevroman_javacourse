package Resources;

public final class OutputResources {
    public static final String INPUT_MONEY = "\nInput amount of money:";
    public static final String INPUT_EXCHANGE_RATE = "\nInput exchange rate:";
    public static final String INPUT_MENU_ITEM = "\nInput menu item:";
    public static final String MENU_ITEMS = "\n1.Add money to balance\n2.Get money from balance\n3.Get converted money from balance\n" +
            "4.Show card balance\n5.Exit";
}

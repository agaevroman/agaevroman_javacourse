package ConsoleProviders;

import Card.Card;
import CardActions.CardActions;
import Resources.ExceptionMessages;
import Resources.OutputResources;

public class UIProvider {
    private final ConsoleReader consoleReader = new ConsoleReader();
    private final CardActions cardActions = new CardActions();

    public void showMessage(String message) {
        System.out.print(message);
    }

    public void showMainMenu(Card card) {
        boolean isRun = true;

        while (isRun) {
            showMessage(OutputResources.MENU_ITEMS);
            switch (consoleReader.getText(OutputResources.INPUT_MENU_ITEM)) {
                case "1" -> cardActions.addMoneyToBalance(card, consoleReader.getBigDecimalValue(OutputResources.INPUT_MONEY));
                case "2" -> cardActions.withdrawMoneyFromBalance(card, consoleReader.getBigDecimalValue(OutputResources.INPUT_MONEY));
                case "3" -> showMessage(cardActions.getConvertedMoney(card, consoleReader.getBigDecimalValue(OutputResources.INPUT_EXCHANGE_RATE)).toString());
                case "4" -> showMessage(card.getBalance().toString());
                case "5" -> isRun = false;
                default -> showMessage(ExceptionMessages.INCORRECT_INPUT);
            }
        }
    }
}

package ConsoleProviders;

import java.math.BigDecimal;
import java.util.Scanner;

import Resources.ExceptionMessages;
import Validators.*;

public class ConsoleReader {
    private final Scanner scanner = new Scanner(System.in);
    private final InputtedValueValidator inputtedValueValidator = new InputtedValueValidator();
    private final ExchangeRateValidator exchangeRateValidator = new ExchangeRateValidator();

    public String getText(String message) {
        System.out.print(message);

        return scanner.nextLine();
    }

    public BigDecimal getBigDecimalValue(String message) throws NumberFormatException {
        String value = getText(message);

        if (inputtedValueValidator.isValid(value)) {
            return BigDecimal.valueOf(Double.parseDouble(value));
        }

        throw new NumberFormatException(ExceptionMessages.INCORRECT_INPUT);
    }

    public BigDecimal getExchangeRate(String message) throws NumberFormatException {
        String value = getText(message);

        if (exchangeRateValidator.isValid(Double.parseDouble(value))) {
            return BigDecimal.valueOf(Double.parseDouble(value));
        }

        throw new NumberFormatException(ExceptionMessages.INCORRECT_INPUT);
    }
}

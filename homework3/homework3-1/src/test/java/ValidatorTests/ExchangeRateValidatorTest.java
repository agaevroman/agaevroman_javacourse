package ValidatorTests;

import Validators.ExchangeRateValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ExchangeRateValidatorTest {
    private final ExchangeRateValidator exchangeRateValidator = new ExchangeRateValidator();

    @Test
    public void testValidatorShouldReturnTrue(){
        double value = 10;

        assertTrue(exchangeRateValidator.isValid(value));
    }

    @Test
    public void testValidatorReturnFalse(){
        double value = 0;

        assertFalse(exchangeRateValidator.isValid(value));
    }
}

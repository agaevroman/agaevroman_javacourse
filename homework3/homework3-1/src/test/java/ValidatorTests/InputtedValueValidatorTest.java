package ValidatorTests;

import Validators.InputtedValueValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InputtedValueValidatorTest {
    private final InputtedValueValidator inputtedStringValidator = new InputtedValueValidator();

    @Test
    public void testValidatorShouldReturnTrue(){
        String value = "12";

        assertTrue(inputtedStringValidator.isValid(value));
    }

    @Test
    public void testValidatorShouldReturnFalse(){
        String value = " 12 ' saefwef";

        assertFalse(inputtedStringValidator.isValid(value));
    }
}

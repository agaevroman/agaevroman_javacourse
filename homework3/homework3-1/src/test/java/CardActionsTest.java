import Card.Card;
import org.junit.jupiter.api.Test;
import CardActions.CardActions;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class CardActionsTest {
    private final Card card =  new Card("Bublik", BigDecimal.valueOf(120));
    private final CardActions cardActions = new CardActions();

    @Test
    public void testAddMoneyShouldWorkAsExpected(){
        BigDecimal money = BigDecimal.valueOf(12);

        cardActions.addMoneyToBalance(card, money);

        BigDecimal actual = card.getBalance();
        BigDecimal expected = BigDecimal.valueOf(132);

        assertEquals(actual, expected);
    }

    @Test
    public void testWithDrawMoneyShouldThrowException(){
        BigDecimal money = BigDecimal.valueOf(130);

        assertThrows(IllegalArgumentException.class, () -> cardActions.withdrawMoneyFromBalance(card, money));
    }

    @Test
    public void testWithDrawMoneyShouldNotThrowException(){
        BigDecimal money = BigDecimal.valueOf(100);

        assertDoesNotThrow(() -> cardActions.withdrawMoneyFromBalance(card, money));
    }

    @Test
    public void testWithDrawMoneyShouldWorkAsExpected(){
        BigDecimal money = BigDecimal.valueOf(100);

        cardActions.withdrawMoneyFromBalance(card, money);

        BigDecimal actual = card.getBalance();
        BigDecimal expected = BigDecimal.valueOf(20);

        assertEquals(actual, expected);
    }

    @Test
    public void testConvertMoneyShouldWorkAsExpected(){
        BigDecimal exchangeRate = BigDecimal.valueOf(2);

        BigDecimal actual = cardActions.getConvertedMoney(card, exchangeRate);
        BigDecimal expected = BigDecimal.valueOf(240);

        assertEquals(actual, expected);
    }
}
